package com.gamefriq.predatorprey;

import java.util.List;

import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.grid.GridCell;
import repast.simphony.query.space.grid.GridCellNgh;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.SimUtilities;

public class Predator {
	private ContinuousSpace<Object> space ;
	private Grid<Object> grid ;
	private boolean moved;
	
	public Predator(ContinuousSpace<Object> space, Grid<Object> grid){
		this.grid = grid;
		this.space = space;
	}
	
	@ScheduledMethod(start=1, interval=1)
	public void step(){
		GridPoint pt = grid.getLocation(this);
		
		GridCellNgh<Prey> nghCreator = new GridCellNgh<Prey>(grid, pt, Prey.class, 1, 1);
		List<GridCell<Prey>> gridCells = nghCreator.getNeighborhood(true);
		SimUtilities.shuffle(gridCells, RandomHelper.getUniform());
		
		GridPoint pointWithMostPrey = null;
		int maxCount = -1;
		for (GridCell<Prey> cell : gridCells){
			if (cell.size() > maxCount) {
				pointWithMostPrey = cell.getPoint();
				maxCount = cell.size();
			}
		}
		
		moveTowards(pointWithMostPrey);
	}
	
	public void moveTowards(GridPoint pt){
		if (!pt.equals(grid.getLocation(this))){
			NdPoint myPoint = space.getLocation(this);
			NdPoint otherPoint = new NdPoint(pt.getX(), pt.getY());
			double angle = SpatialMath.calcAngleFor2DMovement(space, myPoint, otherPoint);
			
			space.moveByVector(this,  1,  angle, 0);
			myPoint = space.getLocation(this);
			grid.moveTo(this, (int)myPoint.getX(), (int)myPoint.getY());
			
			moved = true;
		}
	}
}
